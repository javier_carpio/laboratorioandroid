package com.example.javie.laboratorioandroid;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by javie on 2/21/2018.
 */


public class Persona {

    @ColumnInfo(name = "Cargo")
    private String cargo;

    @ColumnInfo(name = "Nombre")
    private String nombre;

    @ColumnInfo(name = "Edad")
    private int edad;

    @ColumnInfo(name = "DPI")
    private long dpi;

    public Persona(String cargo, String nombre, int edad, long dpi ){
        this.cargo = cargo;
        this.nombre = nombre;
        this.edad = edad;
        this.dpi = dpi;

    }

    public Persona(){}

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setDpi(long dpi) {
        this.dpi = dpi;
    }

    public String getCargo() {
        return cargo;
    }

    public String getNombre() {
        return nombre;
    }

    public int getEdad() {
        return edad;
    }

    public long getDpi() {
        return dpi;
    }

    public String toString(){

        String cadena = cargo + " - " + nombre + " - " + edad + " - " + dpi;

        return cadena;
    }

    public ArrayList<String> toString2(){
        ArrayList<String> per = new ArrayList<>();

        per.add("Cargo: " + getCargo());
        per.add("Nombre: " + getNombre());
        per.add("Edad: " + String.valueOf(getEdad()));
        per.add("DPI: " + String.valueOf(getDpi()));

        return per;
    }

    public Persona persona1(){
        Persona p = new Persona("Papa", "Fabian", 50, 1283291824);



        return p;
    }

    public Persona persona2(){
        Persona p = new Persona("Hijo", "Javier", 18, 1092274928);

        return p;
    }

    public Persona persona3(){
        Persona p = new Persona("Mama", "Sandry", 42, 1293847283);

        return p;
    }

}
