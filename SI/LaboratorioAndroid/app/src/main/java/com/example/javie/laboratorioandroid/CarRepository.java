package com.example.javie.laboratorioandroid;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by javie on 2/28/2018.
 */

public class CarRepository implements ICarroDataSource {

    private ICarroDataSource mLocalDataSource;

    private static CarRepository mInstance;

    public CarRepository(ICarroDataSource mLocalDataSource) {
        this.mLocalDataSource = mLocalDataSource;
    }

    public static CarRepository getmInstance(ICarroDataSource mLocalDataSource){
        if(mInstance == null){
            mInstance = new CarRepository(mLocalDataSource);
        }
        return mInstance;
    }

    @Override
    public Flowable<Carro> getCarroById(int carroId) {
        return mLocalDataSource.getCarroById(carroId);
    }

    @Override
    public Flowable<List<Carro>> getAllCarros() {
        return mLocalDataSource.getAllCarros();
    }

    @Override
    public void insertCarro(Carro... Carros) {
        mLocalDataSource.insertCarro(Carros);
    }

    @Override
    public void updateCarro(Carro... Carros) {
        mLocalDataSource.updateCarro(Carros);
    }

    @Override
    public void deleteCarro(Carro carro) {
        mLocalDataSource.deleteCarro(carro);
    }

    @Override
    public void deleteAllCarros() {
        mLocalDataSource.deleteAllCarros();
    }
}
