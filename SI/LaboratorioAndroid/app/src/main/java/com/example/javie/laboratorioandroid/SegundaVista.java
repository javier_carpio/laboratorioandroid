package com.example.javie.laboratorioandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class SegundaVista extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda_vista);



        Persona p = new Persona();
        Carro car = new Carro();

        Carro c = (Carro)getIntent().getExtras().getSerializable("Carro");
        String marca = getIntent().getStringExtra("Marca");
        String model = getIntent().getStringExtra("Modelo");
        ArrayList<String> per = getIntent().getStringArrayListExtra("Lista");

        TextView textView1 = findViewById(R.id.txt1);
        textView1.setText(marca);
        TextView textView2 = findViewById(R.id.txt2);
        textView2.setText(model);

        ListView lista;
        lista = (ListView)findViewById(R.id.listaView);

        ArrayAdapter adapter;
        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, per);

        lista.setAdapter(adapter);


        Button Regresar = (Button)findViewById(R.id.Regresar);

        Regresar.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                finish();
            }
        });

}


}
