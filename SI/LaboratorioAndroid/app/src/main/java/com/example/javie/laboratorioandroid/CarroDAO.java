package com.example.javie.laboratorioandroid;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by javie on 2/28/2018.
 */

@Dao
public interface CarroDAO {

    @Query("SELECT * FROM Carros WHERE id=:carroId")
    Flowable<Carro> getCarroById(int carroId);

    @Query("SELECT * FROM Carros")
    Flowable<List<Carro>> getAllCarros();

    @Insert
    void insertCarro(Carro... Carros);

    @Update
    void updateCarro(Carro... Carros);

    @Delete
    void deleteCarro(Carro carro);

    @Query("DELETE FROM Carros")
    void deleteAllCarros();
}
