package com.example.javie.laboratorioandroid;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class PrimeraVista extends AppCompatActivity {
    private ListView lista;
    ArrayAdapter adapter;
    List<Carro> carros = new ArrayList<>();

    Persona persona1 = new Persona();
    Carro car = new Carro();
    public int cont = 0;
    public TextView text;

    public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
    private FloatingActionButton fab;

    private CompositeDisposable compositeDisposable;
    private CarRepository carRepository;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primera_vista);

        compositeDisposable = new CompositeDisposable();


        fab = (FloatingActionButton)findViewById(R.id.fab);

        final ArrayList<Carro> carro = new ArrayList<>();

        lista = (ListView)findViewById(R.id.Lista);
        text = (TextView)findViewById(R.id.Texto);

        String[] datos = car.getCarro(carro);

        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, carros);

        lista.setAdapter(adapter);

        AppDatabase appDatabase = AppDatabase.getInstance(this);
        carRepository = CarRepository.getmInstance(CarroDataSource.getInstance(appDatabase.carroDAO()));
        loadData();


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Disposable disposable = Observable.create(new ObservableOnSubscribe<Object>() {
                    @Override
                    public void subscribe(ObservableEmitter<Object> e) throws Exception {

                        Persona pp = new Persona();
                        Carro carro1 = null;
                        cont = cont + 1;

                        if(cont == 1){
                            carro1 = new Carro("BMW", "2015", pp.persona3());
                            carRepository.insertCarro(carro1);

                        }else if(cont == 2){
                            carro1 = new Carro("Mazda", "2017", pp.persona2());
                            carRepository.insertCarro(carro1);
                        }else if(cont == 3){
                            carro1 = new Carro("Toyota", "2016", pp.persona1());
                            carRepository.insertCarro(carro1);
                        }else if(cont == 4){
                            carro1 = new Carro("Honda", "2015", pp.persona2());
                            carRepository.insertCarro(carro1);
                        }else if(cont == 5){
                            carro1 = new Carro("Audi", "2018", pp.persona1());
                            carRepository.insertCarro(carro1);
                        }else{
                            Toast.makeText(getApplicationContext(), "Espacio lleno", Toast.LENGTH_SHORT).show();
                        }
                        e.onComplete();

                    }
                })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(new Consumer() {
                            @Override
                            public void accept(Object o) throws Exception {
                                Toast.makeText(PrimeraVista.this, "Carro anadido!", Toast.LENGTH_SHORT).show();
                            }
                        }, new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                Toast.makeText(PrimeraVista.this, "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }, new Action() {
                            @Override
                            public void run() throws Exception {
                                loadData();
                            }
                        });
            }
        });

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {



                Intent intent = new Intent(PrimeraVista.this, SegundaVista.class);
                Carro carro = (Carro)adapterView.getItemAtPosition(i);

                Toast.makeText(getApplicationContext(), "Click en " + carro.getMarca(), Toast.LENGTH_SHORT).show();

                //intent.putExtra("Carro", (Serializable) carro);
                //startActivityForResult(intent, 1);


                intent.putExtra("Marca", carro.getMarca());
                intent.putExtra("Modelo", carro.getModelo());
                intent.putExtra("Persona", carro.getPersona().toString());
                intent.putStringArrayListExtra("Lista",carro.getPersona().toString2());

                startActivityForResult(intent, 1);
            }
        });
    }

    private void loadData() {
        Disposable disposable = carRepository.getAllCarros()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<Carro>>() {
                    @Override
                    public void accept(List<Carro> carros) throws Exception {
                        onGetAllUserSuccess(carros);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(PrimeraVista.this, ""+throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        compositeDisposable.add(disposable);
    }

    private void onGetAllUserSuccess(List<Carro> carros) {
        this.carros.clear();
        this.carros.addAll(carros);
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_clear:
                deleteAllCarros();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteAllCarros() {
        Disposable disposable = Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(ObservableEmitter<Object> e) throws Exception {
                carRepository.deleteAllCarros();
                cont = 0;
                e.onComplete();

            }
        })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer() {
                    @Override
                    public void accept(Object o) throws Exception {

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(PrimeraVista.this, "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, new Action() {
                    @Override
                    public void run() throws Exception {
                        loadData();
                    }
                });

            }
        }
