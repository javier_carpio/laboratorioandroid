package com.example.javie.laboratorioandroid;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Relation;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.annotations.NonNull;

/**
 * Created by javie on 2/21/2018.
 */
@Entity(tableName = "Carros")
public class Carro implements Serializable{
    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "Marca")
    private String marca;

    @ColumnInfo(name = "Modelo")
    private String modelo;

    @Embedded
    private Persona persona;

    public Carro(){}

    @Ignore
    public Carro(String marca, String modelo, Persona persona){
        this.marca = marca;
        this.modelo = modelo;
        this.persona = persona;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    @Override
    public String toString(){
        return new StringBuilder(marca).append("\n").append(modelo).toString();
    }

    @Ignore
    public String[] getCarro(ArrayList<Carro> car){
        String[] datos = new String[car.size()];

        String cadena = "";
        int pos = 0;
        for(Carro c: car){
            datos[pos] = c.getMarca();
            pos = pos + 1;
        }

        return datos;
    }

    @Ignore
    public String[] getInfo(ArrayList<Persona> per){

        String[] datos = new String[per.size()];

        String cadena = "";
        int pos = 0;
        for(Persona p: per){
            datos[pos] = p.toString();
            pos = pos + 1;
        }

        return datos;
    }


}
