package com.example.javie.laboratorioandroid;

import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by javie on 2/28/2018.
 */

public interface ICarroDataSource {

    Flowable<Carro> getCarroById(int carroId);

    Flowable<List<Carro>> getAllCarros();

    void insertCarro(Carro... Carros);

    void updateCarro(Carro... Carros);

    void deleteCarro(Carro carro);

    void deleteAllCarros();

}
