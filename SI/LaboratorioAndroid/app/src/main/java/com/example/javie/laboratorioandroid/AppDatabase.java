package com.example.javie.laboratorioandroid;

import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

/**
 * Created by javie on 2/28/2018.
 */

@Database(entities = {Carro.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase{

    public final static String DATABASE_NAME = "CarroDB";

    private static AppDatabase miInstance;

    public abstract CarroDAO carroDAO();

    public static AppDatabase getInstance(Context context){
        if(miInstance == null){
            miInstance = Room.databaseBuilder(context, AppDatabase.class, DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return miInstance;
    }
}
