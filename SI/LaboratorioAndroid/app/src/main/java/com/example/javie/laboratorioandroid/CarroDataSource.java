package com.example.javie.laboratorioandroid;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by javie on 2/28/2018.
 */

public class CarroDataSource implements ICarroDataSource {

    private CarroDAO carroDAO;
    private static CarroDataSource mInstance;

    public CarroDataSource(CarroDAO carroDAO){
        this.carroDAO = carroDAO;
    }

    public static CarroDataSource getInstance(CarroDAO carroDAO){
        if(mInstance == null){
            mInstance = new CarroDataSource(carroDAO);
        }
        return mInstance;
    }

    @Override
    public Flowable<Carro> getCarroById(int carroId) {
        return carroDAO.getCarroById(carroId);
    }

    @Override
    public Flowable<List<Carro>> getAllCarros() {
        return carroDAO.getAllCarros();
    }

    @Override
    public void insertCarro(Carro... Carros) {
        carroDAO.insertCarro(Carros);
    }

    @Override
    public void updateCarro(Carro... Carros) {
        carroDAO.updateCarro(Carros);
    }

    @Override
    public void deleteCarro(Carro carro) {
        carroDAO.deleteCarro(carro);
    }

    @Override
    public void deleteAllCarros() {
        carroDAO.deleteAllCarros();
    }
}
